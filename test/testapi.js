const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);
chai.should();

const describe = mocha.describe;
const beforeEach = mocha.beforeEach;
const it = mocha.it;

describe('Test de API de Usuarios', () => {
    beforeEach((done) => {
        require('../server.js');
        done();
    });

    it('Prueba que la API de usuarios response', (done) => {
        chai.request('http://localhost:3000')
            .get('/apitechu/v1/hello')
            .end((err, res) => {
                console.log('GET /apitechu/v1/hello finished');
                if (err) {
                    console.log(err);
                }

                res.should.have.status(200);
                res.body.msg.should.be.eql('Hola desde API TechU!');
                done();
            });
    });

    it('Prueba que la API de devuelve una lista de usuarios correcta', (done) => {
        chai.request('http://localhost:3000')
            .get('/apitechu/v1/users')
            .end((err, res) => {
                console.log('GET /apitechu/v1/users finished');
                if (err) {
                    console.log(err);
                }

                res.should.have.status(200);
                for (const user of res.body.users) {
                    user.should.have.property('email');
                    user.should.have.property('password');
                }
                done();
            });
    });
});

describe('First test', () => {
    it('Test that Duckduckgo works', (done) => {
        chai.request('http://www.duckduckgo.com')
            .get('/')
            .end((err, res) => {
                console.log('Request finished');
                if (err) {
                    console.log(err);
                }

                res.should.have.status(200);
                done();
            });
    });
});
