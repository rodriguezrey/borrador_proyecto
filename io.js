const fs = require('fs');

function writeUserDataToFile(data) {
    const jsonUserData = JSON.stringify(data);
    fs.writeFileSync('./usuarios.json', jsonUserData, 'utf8');
}

module.exports.writeUserDataToFile = writeUserDataToFile;
