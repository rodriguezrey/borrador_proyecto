const users = require('../usuarios.json');
const io = require('../io.js');
const userController = require('./UserController.js');
const bcrypt = require('../bcrypt.js');

const loginV1 = (req, res) => {
    console.log('POST /apitechu/v1/login');
    const result = {
        mensaje: 'Login incorrecto'
    };

    const email = req.body.email;
    const password = req.body.password;

    const index = users.findIndex(user => user.password === password && user.email === email);
    if (index !== -1) {
        users[index].logged = true;
        result.mensaje = 'Login correcto';
        result.idUsuario = users[index].id;
        io.writeUserDataToFile(users);
    }

    res.send(result);
};

const loginV2 = (req, res) => {
    console.log('POST /apitechu/v2/login');
    const result = {
        mensaje: 'Login incorrecto'        
    };

    const email = req.body.email;
    const password = req.body.password;

    userController.findUserByEmail(email).then(user => {
        console.log(user);
        if (!user) {
            res.status(404).send();
        } else if (bcrypt.checkPassword(password, user.password)) {
            userController.updateUserState(user, true);
            result.mensaje = 'Login correcto';
            result.idUsuario = user.id;
            res.send(result);
        } else {
            res.status(403).send(result);
        }
    }).catch(err => {
        console.error(err);
        res.status(500).send();
    });
};

const logoutV1 = (req, res) => {
    console.log('POST /apitechu/v1/logout');
    const result = {
        mensaje: 'logout incorrecto'
    };

    const id = req.body.id;

    const index = users.findIndex(user => user.id === id);
    if (index !== -1 && users[index].logged) {
        delete users[index].logged;
        result.mensaje = 'logout correcto';
        result.idUsuario = users[index].id;
        io.writeUserDataToFile(users);
    }

    res.send(result);
};

const logoutV2 = (req, res) => {
    console.log('POST /apitechu/v2/logout');
    const result = {
        mensaje: 'Logout incorrecto'
    };

    const id = req.body.id;

    userController.findUserById(id).then(user => {
        console.log(user);
        if (!user) {
            res.status(404).send();
        } else if (user.logged) {
            userController.updateUserState(user, false);
            result.mensaje = 'Logout correcto';
            res.send(result);
        } else {
            res.send(result);
        }
    }).catch(err => {
        console.error(err);
        res.status(500).send();
    });
};

module.exports = {
    loginV1: loginV1,
    loginV2: loginV2,
    logoutV1: logoutV1,
    logoutV2: logoutV2
};
