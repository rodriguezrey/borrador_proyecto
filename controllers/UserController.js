const requestJson = require('request-json');
const users = require('../usuarios.json');
const io = require('../io.js');
const bcrypt = require('../bcrypt.js');

const mlabUserBaseURL = 'https://api.mlab.com/api/1/databases/apitechu8edjrr/collections/user';
const mlabAccountBaseURL = 'https://api.mlab.com/api/1/databases/apitechu8edjrr/collections/account';
const mlabAPIKey = 'apiKey=' + process.env.MLAB_API_KEY;

const userClient = requestJson.createClient(mlabUserBaseURL);
const accountClient = requestJson.createClient(mlabAccountBaseURL);

const getUsersV1 = (req, res) => {
    console.log('GET /apitechu/v1/users');

    let result = {};

    const top = req.query.$top;
    const count = req.query.$count;

    const isTop = top !== undefined && !isNaN(top);
    const isCount = count !== undefined && count.toLowerCase() === 'true';

    result.users = isTop ? users.slice(0, top) : users;
    result.count = isCount ? users.length : undefined;

    res.send(result);
};

const getUsersV2 = (req, res) => {
    console.log('GET /apitechu/v2/users');

    userClient.get('?' + mlabAPIKey).then(result => {
        res.send(result.body);
    }).catch(err => {
        console.error(err);
        res.status(500).send();
    });
};

const getUserAccountsV2 = (req, res) => {
    console.log('GET /apitechu/v2/user/:id/accounts');

    const idUser = parseInt(req.params.id);

    const query = 'q={"userId":' + idUser + '}';
    accountClient.get('?' + query + '&' + mlabAPIKey).then(result => {
        console.log(result.body);
        if (result.body.length === 0) {
            res.status(404).send();
        } else {
            res.send(result.body);
        }
    }).catch(err => {
        console.error(err);
        res.status(500).send();
    });
};

const findUserByEmail = (email) => {
    console.log('findUserByEmail');
    const query = 'q={"email":"' + email + '"}';
    return userClient.get('?' + query + '&' + mlabAPIKey).then(result => Promise.resolve(result.body[0]));
};

const findUserById = (id) => {
    console.log('findUserById');
    const query = 'q={"id":' + id + '}';
    return userClient.get('?' + query + '&' + mlabAPIKey).then(result => Promise.resolve(result.body[0]));
};

const updateUserState = (user, logging) => {
    console.log('updateUser');
    const putBody = logging ? '{"$set":{"logged":' + logging + '}}' : '{"$unset":{"logged":""}}';
    const query = 'q={"id":' + user.id + '}';
    return userClient.put('?' + query + '&' + mlabAPIKey, JSON.parse(putBody)).then(result => Promise.resolve(result.body));
};

const getUserByIdV2 = (req, res) => {
    console.log('GET /apitechu/v2/user/:id');

    const idUser = parseInt(req.params.id);

    const query = 'q={"id":' + idUser + '}';
    userClient.get('?' + query + '&' + mlabAPIKey).then(result => {
        const userExists = result.body.length > 0;
        if (userExists) {
            res.send(result.body);
        } else {
            res.status(404).send({ msg: 'Usuario no encontrado' });
        }
    }).catch(err => {
        console.error(err);
        res.status(500).send({ msg: 'Problemas al recuperar el usuario' });
    });
};

const createUserV1 = (req, res) => {
    console.log('POST /apitechu/v1/users');
    console.log(req.body);

    const newUser = {
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email
    };

    users.push(newUser);
    io.writeUserDataToFile(users);

    res.send('Usuario añadido con exito');
};

const createUserV2 = (req, res) => {
    console.log('POST /apitechu/v2/users');
    console.log(req.body);

    const newUser = {
        id: req.body.id,
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        password: bcrypt.hash(req.body.password)
    };

    userClient.post('?' + mlabAPIKey, newUser).then(result => {
        console.log('Usuario guardado con exito');
        res.status(201).send({ msg: 'Usuario creado con exito' });
    }).catch(err => {
        console.error(err);
        res.status(500).send({ msg: 'Problemas al crear el usuario' });
    });
};

const removeUserV1 = (req, res) => {
    console.log('DELETE /apitechu/v1/users/:id');

    let result = 'Usuario no encontrado';

    const idUserToRemove = parseInt(req.params.id);
    const newUsers = users.filter(user => user.id !== idUserToRemove);

    if (newUsers.length !== users.length) {
        io.writeUserDataToFile(newUsers);
        result = 'Usuario eliminado con exito';
    }

    res.send(result);
};

module.exports = {
    getUsersV1: getUsersV1,
    getUsersV2: getUsersV2,
    getUserByIdV2: getUserByIdV2,
    createUserV1: createUserV1,
    createUserV2: createUserV2,
    removeUserV1: removeUserV1,
    findUserByEmail: findUserByEmail,
    findUserById: findUserById,
    updateUserState: updateUserState,
    getUserAccountsV2: getUserAccountsV2
};
