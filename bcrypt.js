const bcrypt = require('bcrypt');

const hash = (data) => {
    console.log('Hashing data');

    return bcrypt.hashSync(data, 10);
};

const checkPassword = (sentPassword, userHashPassword) => {
    console.log('Checking password ');
    return bcrypt.compareSync(sentPassword, userHashPassword);
};

module.exports = {
    hash: hash,
    checkPassword: checkPassword
};
