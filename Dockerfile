# Dockerfile APITECHU

# Imagen raiz
FROM node

# Carpeta trabajo
WORKDIR /apitechu

# Añado archivos de mi aplicacion a imagen
ADD . /apitechu

# Instalo los paquetes necesarios
RUN npm install

# Abrir puerto de la API
EXPOSE 3000

# Comando de inicializacion
CMD ["npm", "start"]