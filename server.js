// Express
const express = require('express');
const app = express();

// Env
require('dotenv').config();

// CORS
var enableCORS = (req, res, next) => {
    // No producción!!!11!!!11one!!1!
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT');
    res.set('Access-Control-Allow-Headers', 'Content-Type');
    next();
};
app.use(enableCORS);

// Body parser
app.use(express.json());
// Port
const port = process.env.PORT || 3000;
app.listen(port);
console.log('API escuchando en el puerto ' + port);

// Controllers
const usersController = require('./controllers/UserController.js');
const authController = require('./controllers/AuthController.js');

// User Routes
app.get('/apitechu/v1/users', usersController.getUsersV1);
app.get('/apitechu/v2/users', usersController.getUsersV2);
app.get('/apitechu/v2/users/:id', usersController.getUserByIdV2);
app.post('/apitechu/v1/users', usersController.createUserV1);
app.post('/apitechu/v2/users', usersController.createUserV2);
app.delete('/apitechu/v1/users/:id', usersController.removeUserV1);

app.get('/apitechu/v2/users/:id/accounts', usersController.getUserAccountsV2);

// Login Routes
app.post('/apitechu/v1/login', authController.loginV1);
app.post('/apitechu/v2/login', authController.loginV2);
app.post('/apitechu/v1/logout', authController.logoutV1);
app.post('/apitechu/v2/logout', authController.logoutV2);

// Hello API
app.get('/apitechu/v1/hello', (req, res) => {
    console.log('GET /apitechu/v1/hello');

    const output = {
        msg: 'Hola desde API TechU!'
    };
    res.send(output);
});

// Monster API
app.post('/apitechu/v1/monstruo/:p1/:p2', (req, res) => {
    console.log('POST /apitechu/v1/monstruo/:p1/:p2');

    console.log('Params');
    console.log(req.params);

    console.log('Query');
    console.log(req.query);

    console.log('Headers');
    console.log(req.headers);

    console.log('Body');
    console.log(req.body);

    res.send();
});
